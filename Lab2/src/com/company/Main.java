package com.company;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Employee emp1 = new Employee("ID: 1234", "Surname: Din",
                "Name: Kevin", 15, "Education: ABC University");
        Employee emp2 = new Employee("ID: 1111", "Surname: Dark",
                "Name: Jacob", 24, "Education: HHJ University");
        Employee emp3 = new Employee("ID: 2222", "Surname: Mark",
                "Name: Chanmin", 54, "Education: TyJ University");
        Employee emp4 = new Employee("ID: 3333", "Surname: Kostr",
                "Name: Odi", 77, "Education: VVN University");

        Employee emp5 = new Employee("ID: 4444", "Surname: Nataha",
                "Name: Minji", 17, "Education: CL University");
        Employee emp6 = new Employee("ID: 5555", "Surname: Lee",
                "Name: Chonsok", 29, "Education: QW University");
        Employee emp7 = new Employee("ID: 6666", "Surname: Hyuning",
                "Name: Bahie", 43, "Education: KFC University");
        Employee emp8 = new Employee("ID: 7777", "Surname: Lee",
                "Name: Felix", 66, "Education: MSB University");

        List<Employee> employeesPlayM = new ArrayList<>();
        employeesPlayM.add(emp1);
        employeesPlayM.add(emp2);
        employeesPlayM.add(emp3);
        employeesPlayM.add(emp4);

        List<Employee> employeesPlayM2 = new ArrayList<>();
        employeesPlayM2.add(emp5);
        employeesPlayM2.add(emp6);
        employeesPlayM2.add(emp7);
        employeesPlayM2.add(emp8);

        ContractEmployee cotrEmpPlayM = new ContractEmployee(employeesPlayM);
        List<ContractEmployee> cotrEmp = new ArrayList<>();
        cotrEmp.add(cotrEmpPlayM);

        SalariedEmployee salarEmpPlayM = new SalariedEmployee(employeesPlayM2);
        List<SalariedEmployee> salarEmp = new ArrayList<>();
        salarEmp.add(salarEmpPlayM);

        Pay playMEnt = new Pay(cotrEmp, salarEmp);

        System.out.println("( " + emp1.employeeID + ", Age: " + emp1.age + ", " + emp1.education + ") "
                + emp1.lastName + ", " + emp1.firstName +" is employee of " + playMEnt.getPlayMEntName());
        System.out.println("( " + emp2.employeeID +   ", Age: " + emp2.age + ", " + emp2.education + ") "
                + emp2.lastName + ", " + emp2.firstName +" is employee of " + playMEnt.getPlayMEntName());
        System.out.println("( " + emp3.employeeID +   ", Age: " + emp3.age + ", " + emp3.education + ") "
                + emp3.lastName + ", " + emp3.firstName +" is employee of " + playMEnt.getPlayMEntName());
        System.out.println("( " + emp4.employeeID +   ", Age: " + emp4.age + ", " + emp4.education + ") "
                + emp4.lastName + ", " + emp4.firstName +" is employee of " + playMEnt.getPlayMEntName());

        System.out.println("( " + emp5.employeeID + ", Age: " + emp5.age + ", " + emp5.education + ") "
                + emp5.lastName + ", " + emp5.firstName +" is employee of " + playMEnt.getPlayMEntName());
        System.out.println("( " + emp6.employeeID +   ", Age: " + emp6.age + ", " + emp6.education + ") "
                + emp6.lastName + ", " + emp6.firstName +" is employee of " + playMEnt.getPlayMEntName());
        System.out.println("( " + emp7.employeeID +   ", Age: " + emp7.age + ", " + emp7.education + ") "
                + emp7.lastName + ", " + emp7.firstName +" is employee of " + playMEnt.getPlayMEntName());
        System.out.println("( " + emp8.employeeID +   ", Age: " + emp8.age + ", " + emp8.education + ") "
                + emp8.lastName + ", " + emp8.firstName +" is employee of " + playMEnt.getPlayMEntName());

    }
}
