package com.company;

public class Employee {
    public int age;
    public String firstName;
    public String lastName;
    public String education;
    public String employeeID;

    public Employee(String employeeID, String lastName, String firstName, int age, String education){
        this.employeeID = employeeID;
        this.lastName = lastName;
        this.firstName = firstName;
        this.age = age;
        this.education = education;
    }

}



