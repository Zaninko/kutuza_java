package com.company;
import java.util.List;

public class ContractEmployee {
    public String federalTaxIDmember;
    private static int nextId = 0;

    private List<Employee> emp;
    public ContractEmployee(List<Employee> emp){
        this.federalTaxIDmember = "federal tax id member" + nextId++;
        this.emp = emp;
    }

    public List<Employee> getEmp(){
        return emp;
    }

}
