package com.company;
import java.util.List;

public class SalariedEmployee {
    public String socialSecurityNumber;
    private static int nextNum = 1;

    private final List<Employee> employees;
    public SalariedEmployee(List<Employee> employees){
        this.socialSecurityNumber = "social Security Number:" + nextNum++;
        this.employees = employees;
    }

    public List<Employee> getEmployees(){
        return employees;
    }

}
