package com.company;
import java.util.List;

public class Pay {
    private final static String name = "Entertainment";
    private List<ContractEmployee> contractEmployees;
    private final List<SalariedEmployee> salariedEmployees;
    public String getPlayMEntName(){
        return this.name;
    }

    public Pay(List<ContractEmployee> contractEmployees, List<SalariedEmployee> salariedEmployees){
        this.contractEmployees = contractEmployees;
        this.salariedEmployees = salariedEmployees;
    }

    public String getPayments(){
        final int numStandard = 100500;
        final int numStandard2 = 570110;
        double pay = 0;
        List<Employee> emp;

        for (ContractEmployee contrEmp: contractEmployees ){
            emp = contrEmp.getEmp();
            for (Employee empl : emp){
                if(empl.age <= 18){
                    final double percent = 0.3;
                    pay = numStandard * percent;
                    System.out.println(empl.employeeID + " got Salary in this month: " + pay + "$");
                }else if(empl.age > 18 && empl.age <= 40){
                    final double percent = 1;
                    pay = numStandard * percent;
                    System.out.println(empl.employeeID + " got Salary in this month: " + pay + "$");
                }else if(empl.age > 40 && empl.age <= 65){
                    final double percent = 0.7;
                    pay = numStandard * percent;
                    System.out.println(empl.employeeID + " got Salary in this month: " + pay + "$");
                }else if(empl.age > 65){
                    final double percent = 0.5;
                    pay = numStandard * percent;
                    System.out.println(empl.employeeID + " got Salary in this month: " + pay + "$");
                }else{
                    System.out.println("input is wrong");
                }

            }
        }

        for(SalariedEmployee salariedEmp: salariedEmployees ){
            emp = salariedEmp.getEmployees();
            for (Employee e : emp){
                if(e.age <= 18){
                    final double percent = 0.3;
                    pay = numStandard2 * percent;
                    System.out.println(e.employeeID + " got Salary in this week: " + pay + "$");
                }else if(e.age > 18 && e.age <= 40){
                    final double percent = 1;
                    pay = numStandard2 * percent;
                    System.out.println(e.employeeID + " got Salary in this week: " + pay + "$");
                }else if(e.age > 40 && e.age <= 65){
                    final double percent = 0.7;
                    pay = numStandard2 * percent;
                    System.out.println(e.employeeID + " got Salary in this week: " + pay + "$");
                }else if(e.age > 65){
                    final double percent = 0.5;
                    pay = numStandard2 * percent;
                    System.out.println(e.employeeID + " got Salary in this week: " + pay + "$");
                }else{
                    System.out.println("input is wrong");
                }

            }
        }
        return "";
    }
}