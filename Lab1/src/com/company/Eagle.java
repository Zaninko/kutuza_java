package com.company;

public class Eagle extends Birds{
    private final String name = "Eagle";
    private final String area = "Steppe area";
    private final String food = "raccoons, coyotes, owls";

    @Override
    public String getEat() {
        return name + " can eat " + food;
    }

    @Override
    public String getFly() {
        return name + " can fly over " + area;
    }

    @Override
    public String getRun() {
        return name + " can\'t run";
    }

    @Override
    public String getSwim() {
        return name + " can\'t swim ";
    }

    @Override
    public String getScream() {
        return name + " can scream 🔊 high-pitched whistling/piping notes";
    }
}
