package com.company;

public class Kiwi extends Birds{
    private final String name = "Kiwi";
    private final String area = "New Zealand";
    private final String food = "plants, worms";

    @Override
    public String getEat() {
        return name + " can eat " + food;
    }

    @Override
    public String getFly() {
        return name + " can\'t fly cuz don\'t has wings";
    }

    @Override
    public String getRun() {
        return name + " can run in " + area;
    }

    @Override
    public String getSwim() {
        return name + " can swim in " + area;
    }

    @Override
    public String getScream() {
        return name + " can scream piercing cry/guttural croak";
    }
}
